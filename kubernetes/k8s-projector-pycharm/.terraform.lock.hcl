# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/coder/coder" {
  version     = "0.6.6"
  constraints = "~> 0.6.0"
  hashes = [
    "h1:W+30GCC7qqeSnBOTZWgGUigAb0dKJXZqZk/vMSc36C4=",
    "zh:03b409e6f5654b29391813e911fc54a24919b3f78b68c670c85e0152a892e59c",
    "zh:13c21963a6425fcee70ba1d8e1ad5f0830e3b025ee96e648a0952e91117975ee",
    "zh:13c948c3d33aa1f29dc5cb9440e98bc4d95125c8a6d631f1fc3930e71a38dc38",
    "zh:5f3ad130752d355fa6a7dd18dcfc7a7ecef51800f2714d45aa5f67905204ca7a",
    "zh:646379439f01767993db8f40f2844548f26d66d0b77f2fd52f50860ee5a1fe07",
    "zh:6b762e08e8bfad10ab0e0e95a08c6acb40dd7ccdf1b76185d9d857a16b1cadb2",
    "zh:7f91453f78fa0bb6da0b166b64e1f8e420e8b7e19715e5424b11f0137723cb50",
    "zh:9643dbcbc62d07081d0407e73c55df0d213ba8806a62120fca043c2cef43f769",
    "zh:af50f35f2c0e2174050d0d9f7f6ad1370c2955faabb7591907fd7dee275f38d0",
    "zh:c2e9fbda1edb5927198606d4bb46250c6eaa0cbb4ec0d63447b90bc54a40febd",
    "zh:cc22578fcac7a06224efa77322bd000cdcd7d738ce32cab0aad0ff852772f780",
    "zh:ce804415e73b5e05af3c5f08c8eae4484638e6a389b20ddb1e05241b19fe8d10",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f93a9384c116ebc2c2ac28e3608ee3060e79f98d05fc6af1fe75778999b158ab",
    "zh:fdfcd7c0abd71be7003ff96dd9d8423e31df71d27f6104fdc0a6531bdd8bb923",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.12.1"
  constraints = "~> 2.12.1"
  hashes = [
    "h1:6ZgqegUao9WcfVzYg7taxCQOQldTmMVw0HqjG5S46OY=",
    "zh:1ecb2adff52754fb4680c7cfe6143d1d8c264b00bb0c44f07f5583b1c7f978b8",
    "zh:1fbd155088cd5818ad5874e4d59ccf1801e4e1961ac0711442b963315f1967ab",
    "zh:29e927c7c8f112ee0e8ab70e71b498f2f2ae6f47df1a14e6fd0fdb6f14b57c00",
    "zh:42c2f421da6b5b7c997e42aa04ca1457fceb13dd66099a057057a0812b680836",
    "zh:522a7bccd5cd7acbb4ec3ef077d47f4888df7e59ff9f3d598b717ad3ee4fe9c9",
    "zh:b45d8dc5dcbc5e30ae570d0c2e198505f47d09098dfd5f004871be8262e6ec1e",
    "zh:c3ea0943f2050001c7d6a7115b9b990f148b082ebfc4ff3c2ff3463a8affcc4a",
    "zh:f111833a64e06659d2e21864de39b7b7dec462615294d02f04c777956742a930",
    "zh:f182dba5707b90b0952d5984c23f7a2da3baa62b4d71e78df7759f16cc88d957",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f76655a68680887daceabd947b2f68e2103f5bbec49a2bc29530f82ab8e3bca3",
    "zh:fadb77352caa570bd3259dfb59c31db614d55bc96df0ff15a3c0cd2e685678b9",
  ]
}

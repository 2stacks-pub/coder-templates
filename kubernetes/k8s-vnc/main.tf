terraform {
  required_providers {
    coder = {
      source  = "coder/coder"
      version = "~> 0.6.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.16.0"
    }
  }
}

provider "kubernetes" {
  # Authenticate via ~/.kube/config or a Coder-specific ServiceAccount, depending on admin preferences
  config_path = var.use_kubeconfig == true ? "~/.kube/config" : null
}

locals {
  image = "codercom/enterprise-vnc:ubuntu"
}

data "coder_workspace" "me" {}

variable "use_kubeconfig" {
  type        = bool
  sensitive   = true
  description = <<-EOF
  Use host kubeconfig? (true/false)

  Set this to false if the Coder host is itself running as a Pod on the same
  Kubernetes cluster as you are deploying workspaces to.

  Set this to true if the Coder host is running outside the Kubernetes cluster
  for workspaces.  A valid "~/.kube/config" must be present on the Coder host.
  EOF
}

variable "namespace" {
  description = "The namespace to create workspaces in (must exist prior to creating workspaces)"
  type        = string
  sensitive   = true
}

variable "cpu" {
  description = "CPU (__ cores), Select 1, 2 or 4"
  type        = string
  default     = "1"
  validation {
    condition = contains([
      "1",
      "2",
      "4",
    ], var.cpu)
    error_message = "Invalid cpu!"
  }
}

variable "memory" {
  description = "Memory (__ GB), Select 2G, 4G or 8G"
  type        = string
  default     = "2G"
  validation {
    condition = contains([
      "2G",
      "4G",
      "8G",
    ], var.memory)
    error_message = "Invalid memory!"
  }
}

variable "home_disk_size" {
  description = "How large would you like your home volume to be (in GB)?"
  type        = number
  default     = 10
  validation {
    condition     = var.home_disk_size >= 1
    error_message = "Value must be greater than or equal to 1."
  }
}

variable "dotfiles_uri" {
  description = <<-EOF
  Dotfiles repo URI (optional)

  See - https://dotfiles.github.io
  EOF
  type        = string
  default     = ""
}

resource "coder_agent" "coder" {
  os             = "linux"
  arch           = "amd64"
  dir            = "/home/coder"
  startup_script = <<EOT
  #!/bin/bash

  # install dotfiles
  ${var.dotfiles_uri != "" ? "coder dotfiles -y ${var.dotfiles_uri}" : ""}

  # start VNC
  echo "Creating desktop..."
  mkdir -p "$XFCE_DEST_DIR"
  cp -rT "$XFCE_BASE_DIR" "$XFCE_DEST_DIR"
  # Skip default shell config prompt.
  cp /etc/zsh/newuser.zshrc.recommended $HOME/.zshrc
  echo "Initializing Supervisor..."
  nohup supervisord

  EOT
}

resource "coder_app" "novnc" {
  agent_id     = coder_agent.coder.id
  slug         = "vnc"
  display_name = "NoVNC Desktop"
  icon         = "/icon/novnc.svg"
  url          = "http://localhost:6081"
  subdomain    = false
  share        = "owner"

  healthcheck {
    url       = "http://localhost:6081/healthz"
    interval  = 5
    threshold = 15
  }
}

resource "kubernetes_pod" "main" {
  count = data.coder_workspace.me.start_count
  depends_on = [
    kubernetes_persistent_volume_claim.home
  ]
  metadata {
    name      = "coder-${data.coder_workspace.me.owner}-${data.coder_workspace.me.name}"
    namespace = var.namespace
  }
  spec {
    security_context {
      run_as_user = "1000"
      fs_group    = "1000"
    }
    container {
      name    = "coder-container"
      image   = local.image
      command = ["sh", "-c", coder_agent.coder.init_script]
      security_context {
        run_as_user = "1000"
      }
      env {
        name  = "CODER_AGENT_TOKEN"
        value = coder_agent.coder.token
      }
      resources {
        requests = {
          cpu    = "1"
          memory = "2G"
        }
        limits = {
          cpu    = var.cpu
          memory = var.memory
        }
      }
      volume_mount {
        mount_path = "/home/coder"
        name       = "home-directory"
      }
    }
    volume {
      name = "home-directory"
      persistent_volume_claim {
        claim_name = kubernetes_persistent_volume_claim.home.metadata.0.name
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "home" {
  metadata {
    name      = "coder-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}-home"
    namespace = var.namespace
  }
  wait_until_bound = false
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "${var.home_disk_size}Gi"
      }
    }
  }
}

resource "coder_metadata" "workspace_info" {
  count       = data.coder_workspace.me.start_count
  resource_id = kubernetes_pod.main[0].id
  item {
    key   = "CPU"
    value = "${var.cpu} cores"
  }
  item {
    key   = "memory"
    value = var.memory
  }
  item {
    key   = "disk"
    value = var.home_disk_size
  }
  item {
    key   = "image"
    value = local.image
  }
  item {
    key   = "volume"
    value = kubernetes_pod.main[0].spec[0].container[0].volume_mount[0].mount_path
  }
}

# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/coder/coder" {
  version     = "0.6.21"
  constraints = "~> 0.6.0"
  hashes = [
    "h1:nLw+qf52qP5ptBYO9ziAdrHKbvapoPP3e17Jmlx/3ms=",
    "zh:0d91504fe1a3a21f160767e1dfe9c405cbf3c9e7a02c38118e4307171ffaf6f7",
    "zh:17ba5f3b1ca2198e692d66517d8401a8945caaaa45729620cab04510ab280445",
    "zh:1abf824c7c46fd682cbc655b904894162449a48fa6b6cebd57f9bcc93ee499bd",
    "zh:3a35d9d70a50c0029d4ab39720f52036197065f45a72f47bca2c61f1068dca0d",
    "zh:45d6f882838d192360d5527a7c54b24c416563eee88a44f40682af6abd92d624",
    "zh:62ad0c3542f2dbc3eb0e6e831954894d8c61fc9a41ba6b416e53e9d17df386d8",
    "zh:84d68ed76108be08336485ae30eeb57d6c25cfadb1d49e8842e82fc84947fd17",
    "zh:88ccee050b5cf7a8ba4031dfac2b78c0272946d7a74d7747a1c37a0f2a8e261c",
    "zh:8d00fa8797656d88dff0fb0a13c71bb7c8794caffe6994c853ff51908de57f57",
    "zh:a79e740d0a4e1635ef65d71e74795d3eef7c3cb06a2c623bf0c42cf8b201374b",
    "zh:bf11355dc296c929a7b77afdb6a2c53e94628f4ce2173333e89347863ec5e500",
    "zh:d2c61f953ff57c9cf211384b33505b5ff63fbb52a6a8d564013ab9bb7efd2444",
    "zh:e07c25fe6405aa5bb269aff0d5dd9935c573c6b36965a85a0e950db70f18c862",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fad1de63fe0b673ab11773ceb1ff08603fe600f34d4c82a2944be030febb4057",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.16.1"
  constraints = "~> 2.16.0"
  hashes = [
    "h1:i+DwtJK82sIWmTcQA9lL0mlET+14/QpUqv10fU2o3As=",
    "zh:06224975f5910d41e73b35a4d5079861da2c24f9353e3ebb015fbb3b3b996b1c",
    "zh:2bc400a8d9fe7755cca27c2551564a9e2609cfadc77f526ef855114ee02d446f",
    "zh:3a479014187af1d0aec3a1d3d9c09551b801956fe6dd29af1186dec86712731b",
    "zh:73fb0a69f1abdb02858b6589f7fab6d989a0f422f7ad95ed662aaa84872d3473",
    "zh:a33852cd382cbc8e06d3f6c018b468ad809d24d912d64722e037aed1f9bf39db",
    "zh:b533ff2214dca90296b1d22eace7eaa7e3efe5a7ae9da66a112094abc932db4f",
    "zh:ddf74d8bb1aeb01dc2c36ef40e2b283d32b2a96db73f6daaf179fa2f10949c80",
    "zh:e720f3a15d34e795fa9ff90bc755e838ebb4aef894aa2a423fb16dfa6d6b0667",
    "zh:e789ae70a658800cb0a19ef7e4e9b26b5a38a92b43d1f41d64fc8bb46539cefb",
    "zh:e8aed7dc0bd8f843d607dee5f72640dbef6835a8b1c6ea12cea5b4ec53e463f7",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fb3ac4f43c8b0dfc0b0103dd0f062ea72b3a34518d4c8808e3a44c9a3dd5f024",
  ]
}

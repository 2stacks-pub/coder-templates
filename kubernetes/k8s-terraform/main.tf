terraform {
  required_providers {
    coder = {
      source  = "coder/coder"
      version = "~> 0.6.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.16.0"
    }
  }
}

provider "kubernetes" {
  # Authenticate via ~/.kube/config or a Coder-specific ServiceAccount, depending on admin preferences
  config_path = var.use_kubeconfig == true ? "~/.kube/config" : null
}

variable "use_kubeconfig" {
  type        = bool
  sensitive   = true
  description = <<-EOF
  Use host kubeconfig? (true/false)

  Set this to false if the Coder host is itself running as a Pod on the same
  Kubernetes cluster as you are deploying workspaces to.

  Set this to true if the Coder host is running outside the Kubernetes cluster
  for workspaces.  A valid "~/.kube/config" must be present on the Coder host.
  EOF
}

variable "namespace" {
  description = "The namespace to create workspaces in (must exist prior to creating workspaces)"
  type        = string
  sensitive   = true
}

variable "code_server_version" {
  description = <<-EOF
  What version of code-server would you like to install?

  See - https://github.com/coder/code-server/releases
  EOF
  type        = string
  default     = "4.9.1"
  sensitive   = true
}

variable "cpu" {
  description = "CPU (__ cores)"
  type        = number
  default     = 2
  validation {
    condition = contains([
      2,
      4
    ], var.cpu)
    error_message = "Invalid cpu!"
  }
}

variable "memory" {
  description = "Memory (__ GB)"
  type        = number
  default     = 4
  validation {
    condition = contains([
      2,
      4,
      6,
      8
    ], var.memory)
    error_message = "Invalid memory!"
  }
}

variable "home_disk_size" {
  description = "How large would you like your home volume to be (in GB)?"
  type        = number
  default     = 10
  validation {
    condition     = var.home_disk_size >= 1
    error_message = "Value must be greater than or equal to 1."
  }
}

variable "dotfiles_uri" {
  description = <<-EOF
  Dotfiles repo URI (optional)

  See - https://dotfiles.github.io
  EOF
  type        = string
  default     = ""
}

variable "install_zsh" {
  description = "Install ZSH and Oh-My-ZSH"
  type        = bool
  default     = true
}

variable "terraform_version" {
  description = <<-EOF
  What version of Terraform would you like to install?

  See - https://releases.hashicorp.com/terraform/
  EOF
  type        = string
  default     = "1.3.7"
}

data "coder_workspace" "me" {}

locals {
  code_server_version = trimprefix(var.code_server_version, "v")
  terraform_version   = trimprefix(var.terraform_version, "v")
}

resource "coder_agent" "main" {
  os             = "linux"
  arch           = "amd64"
  startup_script = <<EOT
    #!/bin/bash

    # home folder can be empty, so copying default bash settings
    if [ ! -f ~/.profile ]; then
      cp /etc/skel/.profile $HOME
    fi
    if [ ! -f ~/.bashrc ]; then
      cp /etc/skel/.bashrc $HOME
    fi

    # install and start code-server
    curl -fsSL https://code-server.dev/install.sh | sh -s -- --version ${local.code_server_version} | tee code-server-install.log
    code-server --auth none --port 13337 | tee code-server-install.log &

    # install terraform
    echo -e "⤵ Installing Terraform..."
    wget https://releases.hashicorp.com/terraform/${local.terraform_version}/terraform_${local.terraform_version}_linux_amd64.zip
    unzip terraform_${local.terraform_version}_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
    rm -rf terraform_${local.terraform_version}_linux_amd64.zip
    echo -e "✅ Successfully installed Terraform version: $(terraform --version)"

    # install zsh
    if [ ${var.install_zsh} = true ] ; then
      echo -e "⤵ Installing zsh..."
      sudo apt-get update && sudo apt-get -y install zsh
      echo -e "✅ Successfully installed zsh version: $(zsh --version)"

      # Set up Oh-My-ZSH
      PATH_TO_ZSH_DIR=$HOME/.oh-my-zsh
      echo -e "Checking if .oh-my-zsh directory exists at $PATH_TO_ZSH_DIR..."
      if [ -d $PATH_TO_ZSH_DIR ]
      then
        echo -e "\n$PATH_TO_ZSH_DIR directory exists!\nSkipping installation of oh-my-zsh.\n"
      else
        echo -e "\n$PATH_TO_ZSH_DIR directory not found."
        echo -e "⤵ Configuring oh-my-zsh in the $HOME directory..."
        (cd $HOME && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended)
        echo -e "✅ Successfully installed oh-my-zsh"
      fi

      # Set the default shell
      echo -e "⤵ Changing the default shell"
      sudo chsh -s $(which zsh) $USER
      echo -e "✅ Successfully modified the default shell"
    fi

    # install dotfiles
    ${var.dotfiles_uri != "" ? "coder dotfiles -y ${var.dotfiles_uri}" : ""}
  EOT
}

# code-server
resource "coder_app" "code-server" {
  agent_id     = coder_agent.main.id
  slug         = "code-server"
  display_name = "code-server"
  icon         = "/icon/code.svg"
  url          = "http://localhost:13337?folder=/home/coder"
  subdomain    = false
  share        = "owner"

  healthcheck {
    url       = "http://localhost:13337/healthz"
    interval  = 3
    threshold = 10
  }
}

resource "kubernetes_persistent_volume_claim" "home" {
  metadata {
    name      = "coder-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}-home"
    namespace = var.namespace
  }
  wait_until_bound = false
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "${var.home_disk_size}Gi"
      }
    }
  }
}

resource "kubernetes_pod" "main" {
  count = data.coder_workspace.me.start_count
  metadata {
    name      = "coder-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}"
    namespace = var.namespace
  }
  spec {
    security_context {
      run_as_user = "1000"
      fs_group    = "1000"
    }
    container {
      name              = "dev"
      image             = "codercom/enterprise-base:ubuntu"
      image_pull_policy = "Always"
      command           = ["sh", "-c", coder_agent.main.init_script]
      security_context {
        run_as_user = "1000"
      }
      env {
        name  = "CODER_AGENT_TOKEN"
        value = coder_agent.main.token
      }
      resources {
        requests = {
          cpu    = "500m"
          memory = "500Mi"
        }
        limits = {
          cpu    = var.cpu
          memory = "${var.memory}G"
        }
      }
      volume_mount {
        mount_path = "/home/coder"
        name       = "home"
        read_only  = false
      }
    }
    volume {
      name = "home"
      persistent_volume_claim {
        claim_name = kubernetes_persistent_volume_claim.home.metadata.0.name
        read_only  = false
      }
    }
  }
}

resource "coder_metadata" "deployment" {
  count       = data.coder_workspace.me.start_count
  resource_id = kubernetes_pod.main[0].id
  icon        = "/icon/k8s.svg"
  item {
    key   = "Namespace"
    value = var.namespace
  }
  item {
    key   = "Pod Name"
    value = kubernetes_pod.main[0].metadata[0].name
  }
  item {
    key   = "Image"
    value = kubernetes_pod.main[0].spec[0].container[0].image
  }
  item {
    key   = "Image Pull Policy"
    value = kubernetes_pod.main[0].spec[0].container[0].image_pull_policy
  }
  item {
    key   = "CPU (limits, requests)"
    value = "${var.cpu} cores, ${kubernetes_pod.main[0].spec[0].container[0].resources[0].requests.cpu}"
  }
  item {
    key   = "Memory (limits, requests)"
    value = "${var.memory}GB, ${kubernetes_pod.main[0].spec[0].container[0].resources[0].requests.memory}"
  }
  item {
    key   = "Security Context - container"
    value = "run_as_user ${kubernetes_pod.main[0].spec[0].container[0].security_context[0].run_as_user}"
  }
  item {
    key   = "Security Context - pod"
    value = "run_as_user ${kubernetes_pod.main[0].spec[0].security_context[0].run_as_user} fs_group ${kubernetes_pod.main[0].spec[0].security_context[0].fs_group}"
  }
}

resource "coder_metadata" "pvc" {
  resource_id = kubernetes_persistent_volume_claim.home.id
  icon        = "/icon/folder.svg"
  item {
    key   = "Size"
    value = kubernetes_persistent_volume_claim.home.spec[0].resources[0].requests.storage
  }
  item {
    key   = "Path"
    value = "/home/coder"
  }
}

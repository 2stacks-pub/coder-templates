---
name: Develop in Kubernetes
description: Get started with Kubernetes development.
tags: [cloud, kubernetes]
icon: /icon/k8s.png
---

# Getting started

This template creates a pod running the `codercom/enterprise-base:ubuntu` image.

## RBAC

The Coder provisioner requires permission to administer pods to use this template. The template
creates workspaces in a single Kubernetes namespace, using the `workspaces_namespace` parameter set
while creating the template.

Create a role as follows and bind it to the user or service account that runs the coder host.

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: coder
rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["*"]
```

## Authentication

This template can authenticate using in-cluster authentication, or using a kubeconfig local to the
Coder host. For additional authentication options, consult the [Kubernetes provider
documentation](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs).

### kubeconfig on Coder host

If the Coder host has a local `~/.kube/config`, you can use this to authenticate
with Coder. Make sure this is done with same user that's running the `coder` service.

To use this authentication, set the parameter `use_kubeconfig` to true.

### In-cluster authentication

If the Coder host runs in a Pod on the same Kubernetes cluster as you are creating workspaces in,
you can use in-cluster authentication.

To use this authentication, set the parameter `use_kubeconfig` to false.

The Terraform provisioner will automatically use the service account associated with the pod to
authenticate to Kubernetes. Be sure to bind a [role with appropriate permission](#rbac) to the
service account. For example, assuming the Coder host runs in the same namespace as you intend
to create workspaces:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: coder

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: coder
subjects:
  - kind: ServiceAccount
    name: coder
roleRef:
  kind: Role
  name: coder
  apiGroup: rbac.authorization.k8s.io
```

Then start the Coder host with `serviceAccountName: coder` in the pod spec.

## Namespace

The target namespace in which the pod will be deployed is defined via the `coder_workspace`
variable. The namespace must exist prior to creating workspaces.

## Persistence

The `/home/coder` directory in this example is persisted via the attached PersistentVolumeClaim.
Any data saved outside of this directory will be wiped when the workspace stops.

Since most binary installations and environment configurations live outside of
the `/home` directory, we suggest including these in the `startup_script` argument
of the `coder_agent` resource block, which will run each time the workspace starts up.

For example, when installing the `aws` CLI, the install script will place the
`aws` binary in `/usr/local/bin/aws`. To ensure the `aws` CLI is persisted across
workspace starts/stops, include the following code in the `coder_agent` resource
block of your workspace template:

```terraform
resource "coder_agent" "main" {
  startup_script = <<EOT
    #!/bin/bash

    # install AWS CLI
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
  EOT
}
```

## code-server

`code-server` is installed via the `startup_script` argument in the `coder_agent`
resource block. The `coder_app` resource is defined to access `code-server` through
the dashboard UI over `localhost:13337`.

# Terraform Resources

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_coder"></a> [coder](#requirement\_coder) | ~> 0.6.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.16.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_coder"></a> [coder](#provider\_coder) | 0.6.6 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.16.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [coder_agent.main](https://registry.terraform.io/providers/coder/coder/latest/docs/resources/agent) | resource |
| [coder_app.code-server](https://registry.terraform.io/providers/coder/coder/latest/docs/resources/app) | resource |
| [coder_metadata.deployment](https://registry.terraform.io/providers/coder/coder/latest/docs/resources/metadata) | resource |
| [coder_metadata.pvc](https://registry.terraform.io/providers/coder/coder/latest/docs/resources/metadata) | resource |
| [kubernetes_persistent_volume_claim.home](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume_claim) | resource |
| [kubernetes_pod.main](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/pod) | resource |
| [coder_workspace.me](https://registry.terraform.io/providers/coder/coder/latest/docs/data-sources/workspace) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_code_server_version"></a> [code\_server\_version](#input\_code\_server\_version) | What version of code-server would you like to install?<br><br>See - https://github.com/coder/code-server/releases | `string` | `"4.9.1"` | no |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | CPU (\_\_ cores) | `number` | `2` | no |
| <a name="input_dotfiles_uri"></a> [dotfiles\_uri](#input\_dotfiles\_uri) | Dotfiles repo URI (optional)<br><br>See - https://dotfiles.github.io | `string` | `""` | no |
| <a name="input_home_disk_size"></a> [home\_disk\_size](#input\_home\_disk\_size) | How large would you like your home volume to be (in GB)? | `number` | `10` | no |
| <a name="input_install_zsh"></a> [install\_zsh](#input\_install\_zsh) | Install ZSH and Oh-My-ZSH | `bool` | `true` | no |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory (\_\_ GB) | `number` | `4` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | The namespace to create workspaces in (must exist prior to creating workspaces) | `string` | n/a | yes |
| <a name="input_terraform_version"></a> [terraform\_version](#input\_terraform\_version) | What version of Terraform would you like to install?<br><br>See - https://releases.hashicorp.com/terraform/ | `string` | `"1.3.7"` | no |
| <a name="input_use_kubeconfig"></a> [use\_kubeconfig](#input\_use\_kubeconfig) | Use host kubeconfig? (true/false)<br><br>Set this to false if the Coder host is itself running as a Pod on the same<br>Kubernetes cluster as you are deploying workspaces to.<br><br>Set this to true if the Coder host is running outside the Kubernetes cluster<br>for workspaces.  A valid "~/.kube/config" must be present on the Coder host. | `bool` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

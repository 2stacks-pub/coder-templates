# coder-templates
Templates are written in Terraform and describe the infrastructure for workspaces (e.g., docker_container, aws_instance, kubernetes_pod).

[Coder Docs](https://coder.com/docs/coder-oss/latest/templates)

## Get the CLI
The CLI and the server are the same binary. We did this to encourage virality so individuals can start their own Coder deployments.

From your local machine, download the CLI for your operating system from the releases or run:
```bash
curl -fsSL https://coder.com/install.sh | sh
```

To see the sub-commands for managing templates, run:
```bash
coder templates --help
```

## Login to your Coder Deployment
Before you can create templates, you must first login to your Coder deployment with the CLI.
```bash
coder login https://coder.example.com # aka the URL to your coder instance
```

> Make a note of the API Key. You can re-use the API Key in future CLI logins or sessions.
```bash
coder --token <your-api-key> login https://coder.example.com/ # aka the URL to your coder instance
```

## Add a template
Before users can create workspaces, you'll need at least one template in Coder.
```bash
# create a local directory to store templates
mkdir -p $HOME/coder/templates
cd $HOME/coder/templates

# start from an example
coder templates init

# optional: modify the template
vim <template-name>/main.tf

# add the template to Coder deployment
coder templates create <template-name>
```

> See the documentation and source code for each example as well as community templates in the [examples/](https://github.com/coder/coder/tree/main/examples/templates) directory in the repo.

## Edit the metadata of a template
To control cost, specify a maximum time to live flag for a template in hours or minutes.
```bash
coder templates edit my-template --default-ttl 4h
```
Edit additonal template settings
```bash
coder templates edit my-template --description string
coder templates edit my-template --display-name string
coder templates edit my-template --icon string
```

## create a workspace from the template; specify any variables
```bash
coder create --template="<templateName>" <workspaceName>
```

## show the resources behind the workspace and how to connect
```bash
coder show <workspace-name>
```

## Push a new template version
```bash
coder templates push <template-name>
```

## Update a workspace
```bash
coder update <your workspace name> --always-prompt
```

# License
Copyright (c) 2023 [2stacks.net](https://www.2stacks.net)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

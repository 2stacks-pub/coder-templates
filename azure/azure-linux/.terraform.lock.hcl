# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/coder/coder" {
  version     = "0.6.0"
  constraints = "0.6.0"
  hashes = [
    "h1:IKimeW0JXyHiAm3b/JeMI0DsE+W8PffeOlJuPbgAnbw=",
    "zh:1bcb3307583d0b2590b6c1cba259adfbedd4affe17e72c3c85960990525a6a71",
    "zh:2584cf99aec4db94eb2954d06a6d3580b986fd2863b5a1ce3a4e24e5e9f7fc3c",
    "zh:3daf7dc313b2ef56c271282a3d891f60de1d12a4b1f4ecd0687cdbe01d9067b6",
    "zh:4c004b33b660bd3c55fa2fd9283edd1add219d2c38e581f5aadc84e33599ec1b",
    "zh:78dd1548841c61a015cb19c6af64f66007413876cd377ff15c8d326d133a1f82",
    "zh:88166cd1cf6ea9d0ac71fb4d98f1cbc897f687b5b8d0b644056b63f3431ff1b7",
    "zh:9327aa24cd867824d3603871afff2c1f9b0fe6a4b440a044217844b07dbda05b",
    "zh:a48f8970f657f38697f1dd1d55e5ab3dfdee0087df36eca17f2545f1876535b6",
    "zh:ae388dc6da037662916337c583e15586ef20bbe233e5e6e8eaa55c53e27b3370",
    "zh:d4217132da03b75528a45de8f39c64f4688b7d81e9c1bdf28a959a2becc8f2ad",
    "zh:e2d15ebed88af1fc60d8e75e86cc3b88a327cfb7850bbb96041966cd1ba6480e",
    "zh:e71312a701a96a68d1d6e42254ec8bd57ae3fd1212a8240d942fc196ed734f2f",
    "zh:ee97dd50794fab5cff345c205e5b260c7af481ac359c8caf0b9c7152bcc7cf67",
    "zh:f47f398cb5cd748183c0056ea029a1cedd5d0bb517f993b7686d5e2bd7f2d699",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "3.0.0"
  constraints = "3.0.0"
  hashes = [
    "h1:yZB4RESHY23SOtMyFTXaz7cBaTKK5UnDn/rhIbNgWYg=",
    "zh:23a039a606cc194594f7c15cd8deef15c5183e11a40e96adee2f7317dbfa18aa",
    "zh:414890618efc6caccf60b81fcce18a7e69a6d81599678d24f538d53726f49c57",
    "zh:7c9a5d3c416766c6f624e186ee2f5b216dd5a9ffef40bfea42ceccf2b217e0d3",
    "zh:82bbeaa6e10d0834d05c2ea55182ce6e147299b1257b445327ff6ff9dfdff3e7",
    "zh:96d5f7737a3d10cc25815f1a220ef8ffe3641ee3229c7738804dc8cff71663fa",
    "zh:ac359915e11a4fa234476cca5e701631ba563d8192dd3f1d31b51674411a0394",
    "zh:bdf07291bb4f41ba304f12b298a066ac70925b3749c01aa90276727cfb0b2662",
    "zh:cf7b4f9c313155b7d5c98e0cbbcfec40c789fccf431875b4db630e9e58f3ae6c",
    "zh:d1fd0d3a1017427ab6f4fadb3310b4b488ab020a541778653c03c51e5e1df809",
    "zh:db946fc8cfc15abe18314dc3dbcbb630243dc34c29f81a728e1397d797dca6a0",
    "zh:e07f73c2745b56043d8b779f2987eb1a5f812645db6ac8fa7878ad23f6a79459",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "4.0.4"
  hashes = [
    "h1:pe9vq86dZZKCm+8k1RhzARwENslF3SXb9ErHbQfgjXU=",
    "zh:23671ed83e1fcf79745534841e10291bbf34046b27d6e68a5d0aab77206f4a55",
    "zh:45292421211ffd9e8e3eb3655677700e3c5047f71d8f7650d2ce30242335f848",
    "zh:59fedb519f4433c0fdb1d58b27c210b27415fddd0cd73c5312530b4309c088be",
    "zh:5a8eec2409a9ff7cd0758a9d818c74bcba92a240e6c5e54b99df68fff312bbd5",
    "zh:5e6a4b39f3171f53292ab88058a59e64825f2b842760a4869e64dc1dc093d1fe",
    "zh:810547d0bf9311d21c81cc306126d3547e7bd3f194fc295836acf164b9f8424e",
    "zh:824a5f3617624243bed0259d7dd37d76017097dc3193dac669be342b90b2ab48",
    "zh:9361ccc7048be5dcbc2fafe2d8216939765b3160bd52734f7a9fd917a39ecbd8",
    "zh:aa02ea625aaf672e649296bce7580f62d724268189fe9ad7c1b36bb0fa12fa60",
    "zh:c71b4cd40d6ec7815dfeefd57d88bc592c0c42f5e5858dcc88245d371b4b8b1e",
    "zh:dabcd52f36b43d250a3d71ad7abfa07b5622c69068d989e60b79b2bb4f220316",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}
